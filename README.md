> 五分钟 Markdown —— [连享会](https://www.lianxh.cn)

&emsp; 

>> 你可以点击右上角【Fork】按钮，把本项目 Fork 到你的码云主页下。定期强制同步，可以实现与本项目主页的同步更新。 你的码云主页下可以自建很多新项目。

&emsp;
- 五分钟 Markdown 视频：[新浪微博](https://weibo.com/tv/v/IzkIK5mHr?fid=1034:4484204327796746)；[短书直播](https://lianxh.duanshu.com/#/brief/video/1ad258af4a5b429d8edf2f20e843da2b)
- [Wikis](https://gitee.com/arlionn/md/wikis/Home) 里有详细的教程
- [幻灯片](https://gitee.com/arlionn/md/raw/master/%E8%BF%9E%E7%8E%89%E5%90%9B-%E4%BA%94%E5%88%86%E9%92%9FMarkdown-%E5%B9%BB%E7%81%AF%E7%89%87.pdf) [曲奇云盘版](https://quqi.gblhgk.com/s/880197/hUkcZsC5wH4eWAgF) 
- 用 Markdown 写一个幻灯片
  - 在谷歌浏览器中输入 [web.marp.app](web.marp.app)
  - 单击 | [「五分钟Markdown-幻灯片-原始文档」](https://gitee.com/arlionn/md/blob/master/%E4%BA%94%E5%88%86%E9%92%9FMarkdown-%E5%8E%9F%E5%A7%8B%E6%96%87%E6%A1%A3.do)，复制里面的文字。(或者，点击 [这里](https://gitee.com/arlionn/md/attach_files/352673/download) 下载)
  - 将上述文档的内容贴入 [web.marp.app](web.marp.app)，即可看到漂亮的幻灯片
  - 依次点击左上角的「**蓝色三角形图标**」&rarr;「**Print / Export to PDF**」 即可输出 PDF 格式的幻灯片。 
- Marp 幻灯片展示
  - 连玉君，[我的甲壳虫-直播课-幻灯片](https://gitee.com/arlionn/paper101/wikis/%E5%B9%BB%E7%81%AF%E7%89%87.md?sort_id=1985074)
  - 连玉君，[动态面板数据模型-幻灯片](https://gitee.com/arlionn/Live/tree/master/%E8%BF%9E%E7%8E%89%E5%90%9B-%E5%8A%A8%E6%80%81%E9%9D%A2%E6%9D%BF%E6%A8%A1%E5%9E%8B)


&emsp;

---

> #### **连享会-直播间** 上线了！             
> #### <http://lianxh.duanshu.com>         
> <img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">   
       
> 长按二维码观看最新直播课     
> 免费公开课：「[直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)」- 连玉君，时长：1小时40分钟。   
> 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)

---
## 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| Python+Github | 司继春 | [Python和Github入门](https://lianxh.duanshu.com/#/brief/course/132a12b2e5ef45b795bfa897c037a6f4) <br> 2小时, 9.9元
| **文本/爬虫** | 游万海<br>司继春 | [4天直播-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 2020.3.28-29日; 4.4-5日 |
| **空间计量** | 范巧    | 已上线， [T1. 空间计量全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e) |
|     |     | 已上线，[T2. 地理加权回归模型(GWR)](https://lianxh.duanshu.com/#/brief/course/a62ca9dcb276496097922a61fcf1a701) |
|     | [T2-T5全程](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)      | 3月21日，[T3. 内生时空权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) |
| 动态模型    |     | 4月05日，[T4. 空间面板模型及动态设定](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3) |
| 空间DID    |     | 4月11日，[T5. 双重差分空间计量模型(SDID)](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 论文重现 | 连玉君    | [经典论文精讲](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)，[-课程资料-](https://gitee.com/arlionn/Paper101), [-幻灯片-](https://quqi.com/s/880197/nz9LvbzzEEpWBNrD)   |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |
| 大数据 | 李兵 | [经济学中的大数据应用](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26) |
| R 语言 | 游万海 | [R 语言初识](https://lianxh.duanshu.com/#/brief/course/a719037536de4812a630a599f8cd7b43), 9.9元

>> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">


&emsp;


> Stata连享会 &ensp;   [课程主页](https://www.lianxh.cn/news/46917f1076104.html)  || [直播视频](http://lianxh.duanshu.com) || [知乎推文](https://www.zhihu.com/people/arlionn/) 

![](https://images.gitee.com/uploads/images/2020/0318/180139_f463ea97_1522177.png)
> 扫码查看连享会最新专题、公开课视频和 100 多个码云计量仓库链接。